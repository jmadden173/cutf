import os

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))

flags = [
    "-x", "c",
    "-std=c89",
    "-Wall",
    "-I./include",
]

def Settings(**kwargs):
    return {
        "flags": flags
    }
