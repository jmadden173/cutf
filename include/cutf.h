/**
 * @file
 * @brief Implements a C Unit Test Framework for c89 standard.
 * @author John Madden
 * @date Created 2021-02-01
 *
 * This library implements a C Unit Test Framework (CUTF) that complies with
 * c89 standards. This library was created to be used with unit testing PIC32
 * microcontroller code. Because of this there is no use of stdlib.h memory
 * management functions as the cost overhead for running on microcontrollers
 * is considered to be too great. It may be required to to adjust MAX_NAME_LEN
 * and MAX_TEST_CASES to meet specific use cases.
 */

#ifndef CUTF_H
#define CUTF_H

#include <stdio.h>
#include <string.h>

#ifdef CUTF_ANSI_SUPPORT
/** Color Red */
#define ANSI_COLOR_RED     "\x1b[31m"
/** Color Green */
#define ANSI_COLOR_GREEN   "\x1b[32m"
/** Color Yellow */
#define ANSI_COLOR_YELLOW  "\x1b[33m"
/** Color Blue */
#define ANSI_COLOR_BLUE    "\x1b[34m"
/** Color Magenta */
#define ANSI_COLOR_MAGENTA "\x1b[35m"
/** Color Cyan */
#define ANSI_COLOR_CYAN    "\x1b[36m"
/** Color Reset */
#define ANSI_COLOR_RESET   "\x1b[0m"
#endif

/** Maximum number of characters for names */
#define MAX_NAME_LEN 80

/** Maximun number of registered test cases */
#define MAX_TEST_CASES 64

/**
 * Creates a new test suite
 *
 * @param name The name of test suite
 */
#define CUTF_SUITE(name) cutf_suite_t _cutf_suite = cutf_suite_create(name)

/**
 * @brief Runs the default test suite.
 *
 * Runs the default test suite and returns value from cutf_suite_rc. This is
 * most likely the number of failed test cases.
 *
 * @see CUTF_SUITE
 */
#define CUTF_SUITE_RUN() cutf_suite_run(&_cutf_suite); \
    return cutf_suite_rc(_cutf_suite)

/**
 * Standard definition of case function.
 *
 * @param name Name of function.
 */
#define CUTF_CASE(name) void name(cutf_case_t* _cutf_case)

/** 
 * @brief Test a expression
 *
 * This macro must be used with CUTF_CASE as the 2 macros are linked by same
 * variables. See cutf_test_expr.
 *
 * @param expr Expression to evaluate.
 */
#define CUTF_TEST(expr) \
    cutf_test_expr(_cutf_case, expr, #expr, __FILE__,__LINE__)

/**
 * Adds a new test case.
 *
 * @param func Function name in nonstring format.
 */
#define ADD_CASE(func) cutf_suite_add_case(&_cutf_suite, #func, &func)

/**
 * Struct for case
 */
typedef struct cutf_case_t cutf_case_t;

/**
 * A typedef for functions that represent cases.
 */
typedef void (*cutf_case_func_t)(cutf_case_t*);

struct cutf_case_t {
    /** Name of test case */
    char name[MAX_NAME_LEN];
    /** Pointer to test function */
    cutf_case_func_t func;
    /** Number of failed tests */
    int failed;
    /** Number of ran tests */
    int ran;
};

/**
 * Struct for a cutf Suite
 */
typedef struct {
    /** Name of cutf suite */
    char name[MAX_NAME_LEN];
    /** Number of test cases */
    int count;
    /** Array of cutf test cases */
    cutf_case_t cases[MAX_TEST_CASES];
} cutf_suite_t;


/**
 * Creates a new test suite with a provided name.
 *
 * @param[in] name Name of suite concatenate to MAX_NAME_LEN.
 * @return New unit testing suite.
 */
cutf_suite_t cutf_suite_create(const char* name);


/**
 * Adds a new case to the testing suite.
 *
 * @param[in,out] cutf_suite Suite to add case to.
 * @param[in] name The name of the case.
 * @param[in] cutf_case_func The function associate with case.
 * @return None
 */
void cutf_suite_add_case(cutf_suite_t* cutf_suite, const char* name,
        cutf_case_func_t cutf_case_func);


/**
 * @brief Runs a test suite.
 *
 * Loops through all test cases where errors get printed out as they occur.
 *
 * @param[in] cutf_suite Testing suite to run.
 * @return None
 */
void cutf_suite_run(cutf_suite_t* cutf_suite);


/**
 * @brief Gets the return code from a suite.
 *
 * The return code indicates whether the suite has passed or failed.
 * Return of 0 indicates a passed suite. Any other value is considered failed.
 *
 * @param[in] cutf_suite The suite to get return code.
 * @return Return code indicating pass or fail.
 */
int cutf_suite_rc(cutf_suite_t cutf_suite);


/**
 * Initializes a new test case.
 *
 * @param[out] cutf_case Case to initialize.
 * @param[in] name Name of test case.
 * @param[in] cutf_case_func Test case function.
 * @return None.
 */
void cutf_case_init(cutf_case_t* cutf_case, const char* name,
        cutf_case_func_t cutf_case_func);


/**
 * Test validity of the result of a expression.
 *
 * @param[in,out] cutf_case Case the test is a part of.
 * @param[in] res Result of the expression.
 * @param[in] expr String of expression.
 * @param[in] file Filename test is a part of.
 * @param[in] line Line the test occurs in.
 * @return None
 */
void cutf_test_expr(cutf_case_t* cutf_case, int res, const char* expr,
        const char* file, int line);

#endif // CUTF_H
