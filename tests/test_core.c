#include <string.h>

#include "cutf.h"

#define TEST_CORE(func) if (func() != 0) { \
    printf(#func); \
    return 1; \
}


void sample_function(cutf_case_t* cutf_case) {}


void sample_func_true(cutf_case_t* cutf_case) {
   cutf_test_expr(cutf_case, 1, "1", __FILE__, __LINE__); 
}


void sample_func_false(cutf_case_t* cutf_case) {
    cutf_test_expr(cutf_case, 0, "0", __FILE__, __LINE__);
}


int test_suite_create() {
    const char name[] = "Hello World";
    cutf_suite_t cutf_suite = cutf_suite_create(name);

    // check zero count
    if (cutf_suite.count != 0) {
        return 1;
    }

    // check matching strings
    if (strcmp(cutf_suite.name, "Hello World") != 0) {
        return 1;
    }

    return 0;
}


int test_suite_add_case() {
    const char suite_name[] = "Hello Suite";
    cutf_suite_t cutf_suite = cutf_suite_create(suite_name);

    const char case_name[] = "Hello Case";
    cutf_suite_add_case(&cutf_suite, case_name, &sample_function);

    // check inc cases
    if (cutf_suite.count != 1) {
        return 1;
    }

    return 0;
}


int test_case_init() {
    const char case_name[] = "Hello Case";
    cutf_case_t cutf_case;
    cutf_case_init(&cutf_case, case_name, &sample_function);
    
    // check non null ptr
    if (cutf_case.func == NULL) {
        return 1;
    } 

    // check name
    if (strcmp(cutf_case.name, "Hello Case") != 0) {
        return 1;
    }

    // check zero ran
    if (cutf_case.ran != 0) {
        return 1;
    }

    // check zero failed
    if (cutf_case.failed != 0) {
        return 1;
    }

    return 0;
}


int test_suite_run() {
    cutf_suite_t cutf_suite = cutf_suite_create("Hello Suite");
    cutf_suite_add_case(&cutf_suite, "True Case", &sample_func_true);
    cutf_suite_add_case(&cutf_suite, "False Case", &sample_func_false);
    cutf_suite_run(&cutf_suite);

    cutf_case_t tmp_case;

    // check true case
    tmp_case = cutf_suite.cases[0];

    if (tmp_case.ran != 1) {
        return 1;
    }

    if (tmp_case.failed != 0) {
        return 1;
    }

    // check false case
    tmp_case = cutf_suite.cases[1];
    
    if (tmp_case.ran != 1) {
        return 1;
    }

    if (tmp_case.failed != 1) {
        return 1;
    }

    return 0;
}


int test_suite_rc() {
    int rc;

    // test returning pass
    cutf_suite_t cutf_suite_pass = cutf_suite_create("Passing Suite");
    cutf_suite_add_case(&cutf_suite_pass, "True Case", &sample_func_true);
    cutf_suite_run(&cutf_suite_pass);
    rc = cutf_suite_rc(cutf_suite_pass);
    if (rc != 0) {
        return 1;
    }

    // test returing fail
    cutf_suite_t cutf_suite_fail = cutf_suite_create("Failing Suite");
    cutf_suite_add_case(&cutf_suite_fail, "False Case", &sample_func_false);
    cutf_suite_run(&cutf_suite_fail);
    rc = cutf_suite_rc(cutf_suite_fail);
    if (rc != 1) {
        return 1;
    }

    return 0;
}


int test_test_expr() {
    const char case_name[] = "Hello Case";
    cutf_case_t cutf_case;
    cutf_case_init(&cutf_case, case_name, &sample_function);

    cutf_test_expr(&cutf_case, 0, "0", __FILE__, __LINE__);

    if (cutf_case.ran != 1) {
        return 1;
    }

    if (cutf_case.failed != 1) {
        return 1;
    }

    cutf_test_expr(&cutf_case, 1, "1", __FILE__, __LINE__);

    if (cutf_case.ran != 2) {
        return 1;
    }

    if (cutf_case.failed != 1) {
        return 1;
    }

    return 0;
}


int main(void) {
    TEST_CORE(test_suite_create);
    TEST_CORE(test_case_init);
    TEST_CORE(test_suite_run);
    TEST_CORE(test_suite_rc);
    TEST_CORE(test_test_expr);

    return 0;
}
