#include "cutf.h"

CUTF_CASE(test_true) {
    int i=10;
    CUTF_TEST(i == 10);
}

int main(void) {
    CUTF_SUITE("test_basic");

    ADD_CASE(test_true);

    CUTF_SUITE_RUN();
}
