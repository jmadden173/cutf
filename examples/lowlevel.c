#include "cutf.h"

void test_basic(cutf_case_t* cutf_case) {
    cutf_test_expr(cutf_case, 1, "1", __FILE__, __LINE__);
}

int main(void) {
    cutf_suite_t test_cutf = cutf_suite_create("test_cutf");

    cutf_suite_add_case(&test_cutf, "test_basic", &test_basic);

    cutf_suite_run(&test_cutf);

    return 0;
}
