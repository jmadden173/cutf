#include "cutf.h"

CUTF_CASE(test_true) {
    CUTF_TEST(1);
}

CUTF_CASE(test_false) {
    CUTF_TEST(0);
}

CUTF_CASE(test_false_complex) {
    int i=3;
    CUTF_TEST(i==4);
}

int main(void) {
    CUTF_SUITE("test_basic");

    ADD_CASE(test_true);
    ADD_CASE(test_false);
    ADD_CASE(test_false_complex);

    CUTF_SUITE_RUN();
}
