#include "cutf.h"


cutf_suite_t cutf_suite_create(const char* name) {
    cutf_suite_t cutf_suite;
    cutf_suite.count = 0;
    strncpy(cutf_suite.name, name, MAX_NAME_LEN-1);
    return cutf_suite;
}


void cutf_suite_add_case(cutf_suite_t* cutf_suite, const char* name,
        cutf_case_func_t cutf_case_func)
{
    cutf_case_init(&cutf_suite->cases[cutf_suite->count++],
            name, cutf_case_func);
}


void cutf_suite_run(cutf_suite_t* cutf_suite) {
    // tmp var for case
    cutf_case_t* cutf_case;

    int total_failed = 0;

    printf("Running %d cases in %s...\n", cutf_suite->count, cutf_suite->name);

    int i;
    for (i=0; i < cutf_suite->count; i++) {
        cutf_case = &cutf_suite->cases[i];

        // run case
        cutf_case->func(cutf_case);

        // find total failed
        total_failed += cutf_case->failed;
    }

    // print total errors
    if (total_failed > 0) {
#ifdef CUTF_ANSI
        printf(ANSI_COLOR_RED "\n*** %d errors detected in %s\n"
                ANSI_COLOR_RESET,
                total_failed, cutf_suite->name);
#else
        printf("\n*** %d errors detected in %s\n",
                total_failed, cutf_suite->name);
#endif
    }
    else {
#ifdef CUTF_ANSI_SUPPORT
        printf(ANSI_COLOR_GREEN "\n*** No errors detected in %s\n"
                ANSI_COLOR_RESET,
                cutf_suite->name);
#else
        printf("\n*** No errors detected in %s\n", cutf_suite->name);
#endif
    }
}


int cutf_suite_rc(cutf_suite_t cutf_suite) {
    int failed_total = 0;

    int i;
    for (i=0; i < cutf_suite.count; i++) {
        failed_total += cutf_suite.cases[i].failed;
    }

    return failed_total;
}


void cutf_case_init(cutf_case_t* cutf_case, const char* name,
        cutf_case_func_t cutf_case_func)
{
    strncpy(cutf_case->name, name, MAX_NAME_LEN-1);
    cutf_case->func = cutf_case_func;
    cutf_case->ran = 0;
    cutf_case->failed = 0;
}


void cutf_test_expr(cutf_case_t* cutf_case, int res, const char* expr,
        const char* file, int line)
{
    cutf_case->ran++;
    if (res == 0) {
        cutf_case->failed++;

#ifdef CUTF_ANSI_SUPPORT
        printf("%s(%d): " ANSI_COLOR_RED
                "error: (%s) evaluates to %d\n" ANSI_COLOR_RESET,
                file, line, expr, res);
#else
        printf("%s(%d): error: (%s) evaluates to %d\n", file, line, expr, res);
#endif
    }
}
