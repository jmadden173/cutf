#!/bin/bash

set -e

# relative folder to build in
BUILD_DIR=build

# create build dir if doesnt exists
if [ ! -d "$BUILD_DIR" ]; then
    mkdir $BUILD_DIR
fi
cd $BUILD_DIR

cmake -DCMAKE_BUILD_TYPE=Debug ..
cmake --build .
ctest
